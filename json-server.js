const jsonServer = require('json-server');
const server = jsonServer.create();
const router = jsonServer.router('db.json');
const middlewares = jsonServer.defaults();

server.use(middlewares);
server.use(jsonServer.bodyParser);

server.post('/users', (req, res) => {

  const data = req.body;
  const teacher = JSON.parse(data);
  console.log('Received data:', teacher);

  router.database.get("teachers").push(teacher).write();

  res.json({ result: 'success' });
});

server.use(router);
server.listen(3004, () => {
  console.log('JSON Server is running');
});