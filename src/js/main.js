function closePopup(element){
    let popup = element.parentElement.parentElement.parentElement;
    popup.style.display = "none";
}

function openAddTeacher(){
    let addTeacher = document.querySelector("#add-teacher");
    addTeacher.style.display = "flex";
}

function openTeacherCard(){
    let addTeacher = document.querySelector("#teacher-info");
    addTeacher.style.display = "flex";
}
