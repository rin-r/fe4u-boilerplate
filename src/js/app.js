const testModules = require('./test-module');
require('../css/app.css');
require("leaflet");
const lodash = require("lodash");
const dayjs = require("dayjs");



/** ******** Your code here! *********** */

console.log(testModules.hello);

//All teachers.
let allTeachers = [];

//Teachers, that will be shown in favorite teachers.
let favoriteTeachers = [];

//Teachers, that will be shown in top teachers list.
let topTeachers = [];

//Array, used to store teachers, that will be shown in statisctics table.
//No longer used!
//let statisticsArray = [];

//Search variable, that says if search filtering is aplied
let isSearchApplied = false;

//Table Settings
const itemsPerPage = 10;
//Table variables
let amountOfPages;
let currentPage = 1;

//Teacher card info
let teacherMap;

//Regions with countries (197)
const asia = ['Afghanistan', 'Bahrain', 'Bangladesh', 'Bhutan', 'Brunei', 'Cambodia', 'China', 'East Timor', 'India', 'Indonesia', 'Iran', 'Iraq', 'Israel', 'Japan', 'Jordan', 'Kazakhstan', 'Kuwait', 'Kyrgyzstan', 'Laos', 'Lebanon', 'Malaysia', 'Maldives', 'Mongolia', 'Myanmar', 'Nepal', 'North Korea', 'Oman', 'Pakistan', 'Palestine', 'Philippines', 'Qatar', 'Saudi Arabia', 'Singapore', 'South Korea', 'Sri Lanka', 'Syria', 'Tajikistan', 'Thailand', 'Turkmenistan', 'United Arab Emirates', 'Uzbekistan', 'Vietnam', 'Yemen'];
const europe = ['Albania', 'Andorra', 'Armenia', 'Austria', 'Azerbaijan', 'Belarus', 'Belgium', 'Bosnia and Herzegovina', 'Bulgaria', 'Croatia', 'Cyprus', 'Czech Republic', 'Denmark', 'Estonia', 'Finland', 'France', 'Georgia', 'Germany', 'Greece', 'Hungary', 'Iceland', 'Ireland', 'Italy', 'Latvia', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Macedonia', 'Malta', 'Moldova', 'Monaco', 'Montenegro', 'Netherlands', 'Norway', 'Poland', 'Portugal', 'Romania', 'Russia', 'San Marino', 'Serbia', 'Slovakia', 'Slovenia', 'Spain', 'Sweden', 'Switzerland', 'Turkey', 'Ukraine', 'United Kingdom', 'Vatican City'];
const africa = ['Algeria', 'Angola', 'Benin', 'Botswana', 'Burkina Faso', 'Burundi', 'Cape Verde', 'Cameroon', 'Central African Republic', 'Chad', 'Comoros', 'Republic of the Congo', 'Democratic Republic of the Congo', 'Djibouti', 'Egypt', 'Equatorial Guinea', 'Eritrea', 'Eswatini', 'Ethiopia', 'Gabon', 'Gambia', 'Ghana', 'Guinea', 'Guinea-Bissau', 'Ivory Coast', 'Kenya', 'Lesotho', 'Liberia', 'Libya', 'Madagascar', 'Malawi', 'Mali', 'Mauritania', 'Mauritius', 'Morocco', 'Mozambique', 'Namibia', 'Niger', 'Nigeria', 'Rwanda', 'Sao Tome and Principe', 'Senegal', 'Seychelles', 'Sierra Leone', 'Somalia', 'South Africa', 'South Sudan', 'Sudan', 'Tanzania', 'Togo', 'Tunisia', 'Uganda', 'Zambia', 'Zimbabwe'];
const north_america = ['Anguilla', 'Antigua and Barbuda', 'Aruba', 'Bahamas', 'Barbados', 'Belize', 'Bermuda', 'British Virgin Islands', 'Canada', 'Cayman Islands', 'Costa Rica', 'Cuba', 'Dominica', 'Dominican Republic', 'El Salvador', 'Greenland', 'Grenada', 'Guadeloupe', 'Guatemala', 'Haiti', 'Honduras', 'Jamaica', 'Martinique', 'Mexico', 'Montserrat', 'Netherlands Antilles', 'Nicaragua', 'Panama', 'Puerto Rico', 'Saint Kitts and Nevis', 'Saint Lucia', 'Saint Pierre and Miquelon', 'Saint Vincent and the Grenadines', 'Trinidad and Tobago', 'Turks and Caicos Islands', 'United States', 'US Virgin Islands'];
const south_america = ['Argentina', 'Bolivia', 'Brazil', 'Chile', 'Colombia', 'Ecuador', 'Falkland Islands', 'French Guiana', 'Guyana', 'Paraguay', 'Peru', 'Suriname', 'Uruguay', 'Venezuela'];
const oceania = ['Australia', 'Fiji', 'Kiribati', 'Marshall Islands', 'Micronesia', 'Nauru', 'New Zealand', 'Palau', 'Papua New Guinea', 'Samoa', 'Solomon Islands', 'Tonga', 'Tuvalu', 'Vanuatu'];

//Charts
let ageChart;
let genderChart;
let regionChart;
let specialityChart;

//Пустий шаблон користувача, який заповнюється данними у майбутньому.
const userTemplate = {
    "id": "",
    "favorite": false,
    "course": "",
    "bg_color": "",
    "note": "",
    "gender": "",
    "title": "",
    "full_name": "",
    "city": "",
    "state": "",
    "country": "",
    "postcode": 0,
    "coordinates": { "latitude": "", "longitude": "" },
    "timezone": { "offset": "", "description": "" },
    "email": "",
    "b_date": "",
    "age": 0,
    "phone": "",
    "picture_large": "",
    "picture_thumbnail": "" 
}

//Функція, яка повертає користувачів, створених за шаблоном, використовуючи дані з randomUserMock та additionalUsers.
function getUserArray(sourceArray) {
    let userArray = [];
    let existingNames = [];

    for (let mockUser of sourceArray) {
        let user = parseUserMock(mockUser);
        if (!lodash.includes(existingNames, user.full_name)) {
            userArray.push(user);
            existingNames.push(user.full_name);
        } else {
            let existingUser = lodash.find(userArray, { 'full_name': user.full_name });
            mergeSameUser(existingUser, user);
        }
    }

    return userArray;
}

//Функція яка оброблює randomUserMock та additionalUser, та повертає обʼєкт користувача у вигляді заповненого шаблону.
function parseUserMock(mockUser) {
    let userObject = {...userTemplate};

    const templateKeys = Object.keys(userObject);

    for(let key of templateKeys){
        switch(key){
            case ("id"):
                let idValue = mockUser[key];
                if(idValue){
                    if(isObject(idValue)){
                        let newId = "";
                        if(idValue['name']) newId += String(idValue['name']).trim();
                        if(idValue['value']) newId += String(idValue['value']).trim();
                        userObject[key] = newId;
                    } else if(typeof idValue === 'string') {
                        if(idValue.trim().length > 0){
                            userObject[key] = idValue;
                        }
                    }
                }
                break;

            case ("course"):
                let courseList = ["Mathematics", "Physics", "English", "Computer Science", "Dancing", "Chess", "Biology", "Chemistry", "Law", "Art", "Medicine", "Statistics"];
                userObject[key] = courseList[Math.floor(Math.random() * courseList.length)];
                break;

            case ("title"):
                if(mockUser[key]){
                    userObject[key] = mockUser[key];
                } else if (mockUser['name']) {
                    if(mockUser['name'][key]) userObject[key] = mockUser['name'][key];
                }
                break;

            case ("full_name"):
                if(mockUser[key]){
                    userObject[key] = mockUser[key];
                } else if (mockUser['name']) {
                    let nameValue = mockUser['name'];
                    let fullName = "";
                    if(nameValue[key]) {userObject[key] = nameValue[key]; break;}
                    if(nameValue['first']) fullName += String(nameValue['first']).trim();
                    if(nameValue['last']) fullName += " " + String(nameValue['last']).trim();
                    userObject[key] = fullName;
                }
                break;

            case ("b_date"):
            case ("age"):
                let dobMockUser = mockUser['dob'];
                if(dobMockUser){
                    if(dobMockUser[key]) {
                        userObject[key] = dobMockUser[key];
                    } else if(dobMockUser['date']) {
                        userObject['b_date'] = dobMockUser['date']
                    }
                } else if (mockUser[key]){
                    userObject[key] = mockUser[key];
                }
                break;
            case ("city"):
            case ("state"):
            case ("country"):
            case ("postcode"):
            case ("coordinates"):
            case ("timezone"):
                let locationMockUser = mockUser['location'];
                if(locationMockUser){
                    if(locationMockUser[key]) {
                        userObject[key] = locationMockUser[key];
                    }
                } else if (mockUser[key]){
                    userObject[key] = mockUser[key];
                }
                break;

            case ("picture_large"):
                if(mockUser[key]){
                    userObject[key] = mockUser[key];
                } else if (mockUser['picture']){
                    userObject[key] = mockUser['picture']['large'];
                }
                break;
            
            case ("picture_thumbnail"):
                if(mockUser[key]){
                    userObject[key] = mockUser[key];
                } else if (mockUser['picture']){
                    userObject[key] = mockUser['picture']['thumbnail'];
                }
                break;

            default:
                if(mockUser[key]) userObject[key] = mockUser[key];
                break;
        }
    }
    return userObject;
}

//Функція, яка перевіряє чи є значення обʼєктом.
function isObject(value) {
    return (typeof value === 'object' && !Array.isArray(value) && value !== null);
}

//Функція, яка обʼєднує двох користувачів в одного і заповнює пусті данні існуючими.
function mergeSameUser(main, mergeWith){
    let mergeWithKeys = Object.keys(mergeWith);

    for(let key of mergeWithKeys){
        let currentValue = mergeWith[key];
        if(Object.hasOwn(main, key) && currentValue) {
            if(typeof currentValue === 'string'){
                if(currentValue.length > 0) main[key] = currentValue;
            } else {
                main[key] = currentValue;
            }
        }
    }
}

//Функція, яка повертає масив лише з валідними користувачами.
function validateUserArray(userArray) {
    return userArray.filter((user) => isUserValid(user));
}

//Функція, яка перевіряє чи є користувач валідним.
function isUserValid(user){
    let stringKeys = ["full_name", "note", "state", "city", "country"];

    for(let key of stringKeys){
        if (!checkString(user[key])) return false;
    }

    if (!lodash.isString(user.gender)) return false;
    if (!lodash.isNumber(user.age)) return false;
    if(!isPhoneValid(user.phone)) return false;
    if(!isEmailValid(user.email)) return false;
    if(!isDateValid(user.b_date)) return false;

    return true;
}

function checkString(string) {
    if(!lodash.isString(string)){
        return false;
    } else {
        let firstLetter = string[0];
        if(firstLetter && (firstLetter != firstLetter.toUpperCase())) return false;
    }
    return true;
}

//Функція, яка перевіряє чи є номер валідним.
function isPhoneValid(numberString){
    let regex = "[\+]?[0-9]{0,3}[-\s\.]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}";
    return numberString.match(regex);
}

//Функція, яка перевіряє чи є пошта валідною.
function isEmailValid(emailString){
    return emailString.includes("@");
}

function isDateValid(dateString) {

    let date = new Date(dateString);

    return Date.now() > date.getTime();

}

//Функція, яка повертає масив користувачів, які задовільняють вимогам заданими параметрами.
function filterUsers(array, region, age, gender, favorite, hasPhoto){

    const regionArray = getArrayByRegion(region);

    return array.filter((user) => {
        if (regionArray && !regionArray.includes(user.country)) return false;
        if (age && user.age !== age) return false;
        if (gender && user.gender !== gender) return false;
        if (favorite !== null && (favorite && user.favorite !== favorite)) return false;
        if (hasPhoto && !(user.picture_large)) return false;
        return true;
      });
}

//Returns array by the name of the region
function getArrayByRegion(regionString) {

    switch (regionString) {
        case "Asia":
            return asia;
        case "Europe":
            return europe;
        case "Africa":
            return africa;
        case "North America":
            return north_america;
        case "South America":
            return south_america;
        case "Oceania":
            return oceania;
        default:
            return undefined;
    }
}

//Функція, яка порівнює користувачів по імені за зростанням.
function sortByNameAscending(a, b){
    return a.full_name.localeCompare(b.full_name);
}

//Функція, яка порівнює користувачів по предмету за зростанням.
function sortByCourseAscending(a, b){
    return a.course.localeCompare(b.course);
}

//Функція, яка порівнює користувачів по статі за зростанням.
function sortByGenderAscending(a, b){
    return a.gender.localeCompare(b.gender);
}

//Функція, яка порівнює користувачів по предмету за зростанням.
function sortByCourseDescending(a, b){
    return a.course.localeCompare(b.course) * -1;
}

//Функція, яка порівнює користувачів по статі за зростанням.
function sortByGenderDescending(a, b){
    return a.gender.localeCompare(b.gender) * -1;
}

//Функція, яка порівнює користувачів за віком за зростанням.
function sortByAgeAscending(a, b){
    return a.age - b.age;
}

//Функція, яка порівнює користувачів за датою народження за зростанням.
function sortByBirthdayAscending(a, b){
    return Date.parse(a.b_date) - Date.parse(b.b_date);
}

//Функція, яка порівнює користувачів за країною за зростанням.
function sortByCountryAscending(a, b){
    return a.country.localeCompare(b.country);
}

//Функція, яка порівнює користувачів по імені за спаданням.
function sortByNameDescending(a, b){
    return a.full_name.localeCompare(b.full_name) * -1;
}

//Функція, яка порівнює користувачів за віком за спаданням.
function sortByAgeDescending(a, b){
    return (a.age - b.age) * -1;
}

//Функція, яка порівнює користувачів за датою народження за спаданням.
function sortByBirthdayDescending(a, b){
    return (Date.parse(a.b_date) - Date.parse(b.b_date)) * -1;
}

//Функція, яка порівнює користувачів за країною за спаданням.
function sortByCountryDescending(a, b){
    return a.country.localeCompare(b.country) * -1;
}

//Функція, яка повертає користувачів, які містять стрічку data у полі full_name, note чи age.
function searchUsers(array, data) {
    let resultArray = [];
    data = lodash.toLower(String(data));

    lodash.forEach(array, (element) => {
        if (lodash.includes(lodash.toLower(element.full_name), data) ||
            lodash.includes(lodash.toLower(element.note), data) ||
            lodash.isEqual(element.age, Number(data))) {
            resultArray.push(element);
        }
    });

    return resultArray;
}

//Функція, яка повертає масив користувачів, які задовольняють виразу expression.
function getExpressionArray(array, expression){
    const numberRegex = "[0-9]+";
    let numbers = [...expression.matchAll(numberRegex)];
    numbers = numbers.map((value) => Number(value));
    numbers.sort((a, b) => a - b);

    return array.filter((user) => {
        if (expression.includes(">")) {
            if (expression.includes("=")) {
                if (user.age < numbers[0]) return false;
            } else {
                if (user.age <= numbers[0]) return false;
            }
        } else if (expression.includes("<")) {
            if(expression.includes("=")) {
                if (user.age > numbers[0]) return false;
            } else {
                if (user.age >= numbers[0]) return false;
            }
        } else if (expression.includes("-")) {
            if (user.age < numbers[0] || user.age > numbers[1]) return false;
        }
        return true;
    })
}

//Функція, яка повертає співвідношення усіх користувачів та тих, які задовольняють виразу expression.
function getExpressionRatio(array, expression){
    let filteredArray = getExpressionArray(array, expression);

    return String(filteredArray.length / array.length * 100.0)+"%";
}

//----------------------------------[LAB 4]----------------------------------

//Function, that returns an array with random users
async function getTeachersFromAPI(amount) {

    //Sends request to the random user API and gets response
    const response = await fetch(`https://randomuser.me/api/?results=${amount}`);
    const data = response.json();

    return data;

}

//Function, that adds event listener to "Load more" button
function addLoadMoreListeners() {

    //Getting button element
    let loadButton = document.querySelector("#load-more");

    loadButton.addEventListener("click", () => {

        getTeachersFromAPI(10)
        .then(randomUsers => {

            //Parsing given array into template format
            let recievedArray = getUserArray(randomUsers.results);
            console.log("Recieved 10 random users: ")
            console.log(recievedArray);

            addTeachersArray(recievedArray);
            updateEverything();

        });

    })

}

//Function, that transfers all users from certain array to allTeachers array.
function addTeachersArray(arrayToAdd) {
    
    for(const teacher of arrayToAdd) {
        allTeachers.push(teacher);
    }

}

//Function, that updates every list/table element on the website.
function updateEverything() {

    topTeachers = applyAllFilters(allTeachers);

    updateTeachersList(topTeachers);
    updateTeachersCarousel();

    loadStatsTable();
}

//Function, that sends request with a form to the json-server.
function sendFormData(form) {

    const formData = new FormData(form);
    const data = {};

    formData.forEach((value, key) => {
        data[key] = value;
    });

    fetch('http://localhost:3004/users', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    })
    .then(console.log("Request sent successfully!"))
    .catch((error) => {
        console.error('Error:', error);
    });

}

//----------------------------------[LAB 3]----------------------------------

window.onload = function(event){

    addFiltersListeners();
    addSearchListeners();
    addTeacherFormListeners();
    addLoadMoreListeners();
    initCharts();

    getTeachersFromAPI(50)
    .then(randomUsers => {

        let recievedArray = getUserArray(randomUsers.results);
        //console.log(recievedArray);

        loadTeachersList(recievedArray);
        loadStatsTable();

    });
}

//---------------------------------------------------------------------------
//FUNCTIONS that apply basic functionality to buttons, initialize everything.
//---------------------------------------------------------------------------

//Function, that adds event listeners to buttons related to user search.
function addSearchListeners() {

    //Document elements related to search
    let searchButton = document.querySelector("#search-button");
    let searchField = document.querySelector("#search-field");
    let clearSearchResults = document.querySelector("#clear-search");

    //Adding event listener to cancel search button
    clearSearchResults.addEventListener("click", () => {

        isSearchApplied = false;

        topTeachers = getFilteredTeachers(allTeachers);
        updateTeachersList(topTeachers);
        loadStatsTable();

        document.querySelector("#top-teachers").textContent = "Top Teachers";
        clearSearchResults.style.display = "none";

    });

    searchButton.addEventListener("click", () => {

        //Checking if search input field is not empty
        if (searchField.value) {

            isSearchApplied = true;

            topTeachers = applyAllFilters(allTeachers);
            updateTeachersList(topTeachers);
            loadStatsTable();

            document.querySelector("#top-teachers").textContent = `Results for "${searchField.value}"`
            clearSearchResults.style.display = "block";

        } else {
            alert("Type the correct input for the search field.")
        }

    });
}

//Function, that adds event listeners to filters of top teachers list
function addFiltersListeners() {
    
    //Filter elements
    let ageSelect = document.querySelector("#age");
    let regionSelect = document.querySelector("#region");
    let sexSelect = document.querySelector("#sex");
    let photoOnlyCheckbox = document.querySelector("#photo-only");
    let favoriteOnlyCheckbox = document.querySelector("#favorites-only");

    //Listener function that will be applied to all filter elements
    var listener = function() {

        topTeachers = applyAllFilters(allTeachers);
        updateTeachersList(topTeachers);
        loadStatsTable();

    }

    //Attaching listener to elements
    ageSelect.addEventListener("change", listener);
    regionSelect.addEventListener("change", listener);
    sexSelect.addEventListener("change", listener);
    photoOnlyCheckbox.addEventListener("change", listener);
    favoriteOnlyCheckbox.addEventListener("change", listener);
}

//Function, that loads basic data into top teachers list 
function loadTeachersList(arrayToRender) {

    const teachersList = document.querySelector(".top-list");

    //Receiving all valid users from JSON file
    allTeachers = arrayToRender;
    topTeachers = allTeachers;

    //Creating cards for all teachers and adding them into top teachers list html element
    for (const teacher of allTeachers) {

        let card = getTeacherCard(teacher);
        teachersList.appendChild(card);

        //If teacher is favorite, then adding it to favorite teachers array
        //that will be used to create favorite teachers carousel.
        if(teacher.favorite) favoriteTeachers.push(teacher);

    }

    //Adding cards to fav. teachers carousel.
    updateTeachersCarousel();
}

//Function, that generates certain amount of random colors
function generateRandomColorsArray(amount) {
    let colors = [];

    for (let i = 0; i < amount; i++) {
        const r = Math.floor(Math.random() * 151) + 105;
        const g = Math.floor(Math.random() * 151) + 105;
        const b = Math.floor(Math.random() * 151) + 105;
        
        colors.push(`rgb(${r}, ${g}, ${b})`);
    }
    return colors;
}

//Function, that creates empty charts
function initCharts() {

    //Page elements
    let agePie = document.querySelector("#age-statistics-pie");
    let specialityPie = document.querySelector("#speciality-statistics-pie");
    let genderPie = document.querySelector("#gender-statistics-pie");
    let regionPie = document.querySelector("#nationality-statistics-pie");

    //Creating charts
    ageChart = new Chart(agePie, {
        type: 'pie',
        data: {
          labels: [],
          datasets: [{
            label: 'Amount of users',
            data: [],
            borderWidth: 1
          }]
        },
        options: {
          responsive: true,
          maintainAspectRatio: false,
        }
    });

    specialityChart = new Chart(specialityPie, {
        type: 'pie',
        data: {
          labels: [],
          datasets: [{
            label: 'Amount of users',
            data: [],
            borderWidth: 1
          }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
        }
    });

    genderChart = new Chart(genderPie, {
        type: 'pie',
        data: {
          labels: [],
          datasets: [{
            label: 'Amount of users',
            data: [],
            borderWidth: 1
          }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
        }
    });

    regionChart = new Chart(regionPie, {
        type: 'pie',
        data: {
          labels: [],
          datasets: [{
            label: 'Amount of users',
            data: [],
            borderWidth: 1
          }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
        }
    });
}

//Function, that updates charts with given data
function updateChart(chart, dataObject) {

    chart.data.labels = Object.keys(dataObject);
    chart.data.datasets[0].data = Object.values(dataObject);
    chart.data.datasets[0]['backgroundColor'] = generateRandomColorsArray(Object.keys(dataObject).length);
    
    chart.update();
}

//Function, that loads basic data for statistics table
function loadStatsTable(){

    let ageStats = getAgeStats(topTeachers);
    let specialityStats = getSpecialityStats(topTeachers);
    let genderStats = getGenderStats(topTeachers);
    let regionStats = getRegionStats(topTeachers);

    updateChart(ageChart, ageStats);
    updateChart(specialityChart, specialityStats);
    updateChart(genderChart, genderStats);
    updateChart(regionChart, regionStats);
}

//Function, that returns an object with age statistics
function getAgeStats(teachersArray) {

    let dataObj = {
        "18-31": 0,
        "32-51": 0,
        "52-71": 0,
        "72-91": 0,
        "91+": 0
    }

    lodash.forEach(teachersArray, (teacher) => {
        const age = teacher.age;
        if (age >= 18 && age <= 31) {
            dataObj["18-31"] += 1;
        } else if (age >= 32 && age <= 51) {
            dataObj["32-51"] += 1;
        } else if (age >= 52 && age <= 71) {
            dataObj["52-71"] += 1;
        } else if (age >= 72 && age <= 91) {
            dataObj["72-91"] += 1;
        } else {
            dataObj["91+"] += 1;
        }
    });

    return dataObj;
}

//Function, that returns an object with speciality statistics
function getSpecialityStats(teachersArray) {
    let dataObj = {};

    lodash.forEach(teachersArray, (teacher) => {
        const speciality = teacher.course;
        if (dataObj.hasOwnProperty(speciality)) {
            dataObj[speciality] += 1;
        } else {
            dataObj[speciality] = 1;
        }
    });

    return dataObj;
}

//Function, that returns an object with gender statistics
function getGenderStats(teachersArray) {

    let dataObj = {
        "male": 0,
        "female": 0
    }

    lodash.forEach(teachersArray, (teacher) => {
        const gender = teacher.gender;
        dataObj[gender] += 1;
    });

    return dataObj;
}

//Function, that returns an object with region statistics
function getRegionStats(teachersArray) {

    let dataObj = {
        "Asia": 0,
        "Europe": 0,
        "Africa": 0,
        "North America": 0,
        "South America": 0,
        "Oceania": 0,
    };

    lodash.forEach(teachersArray, (teacher) => {
        const country = teacher.country;
        if (lodash.includes(asia, country)) {
            dataObj["Asia"] += 1;
        } else if (lodash.includes(europe, country)) {
            dataObj["Europe"] += 1;
        } else if (lodash.includes(africa, country)) {
            dataObj["Africa"] += 1;
        } else if (lodash.includes(north_america, country)) {
            dataObj["North America"] += 1;
        } else if (lodash.includes(south_america, country)) {
            dataObj["South America"] += 1;
        } else if (lodash.includes(oceania, country)) {
            dataObj["Oceania"] += 1;
        }
    });

    return dataObj;
}

//Function, that adds listeners to a form buttons
function addTeacherFormListeners() {

    //Teacher rom popup elements
    let addTeacherButtons = document.querySelectorAll(".add-teacher-button");
    let popup = document.querySelector("#add-teacher");
    let closeButton = popup.querySelector(".close-popup");
    let addTeacherForm = popup.querySelector("form");

    //Attaching event listener to close button
    closeButton.addEventListener("click", () => {closePopup(popup)});

    //Attaching event listener to form submitting
    addTeacherForm.addEventListener("submit", (e) => {

        e.preventDefault();
        addTeacherOnSumbit();

    });

    //Attaching event listener to popup opening 
    for (let button of addTeacherButtons){

        button.addEventListener("click", () => {
            showTeacherFormPopup();
        });

    };
}

//-----------------------------------------------
//FUNCTIONS that working with top teachers list.
//-----------------------------------------------

//Function, that creates and appends teacher cards to top teachers list
//array - an array with teacher objects, that will be appended to top teachers list element
function updateTeachersList(array) {
    
    //Erasing existing cards in teachers list
    const teachersList = document.querySelector(".top-list");
    teachersList.innerHTML = "";

    //Creates cards and appends them to teachers list
    for (const teacher of array) {

        let card = getTeacherCard(teacher);
        teachersList.appendChild(card);

    }
}

//Function, that updates fav. teachers carousel, if new fav. teacher was added.
function updateTeachersCarousel() {

    //Erasing existing cards in carousel
    document.querySelector(".carousel-list").innerHTML = "";

    //Creating fav. teachers cards and appending them to carousel
    for (let teacher of favoriteTeachers){

        let card = getTeacherCard(teacher);
        
        //Removing elements, that shouldn't be visible
        card.querySelector(".favorite-teacher").remove();
        card.querySelector(".teacher-card-course").remove();

        document.querySelector(".carousel-list").appendChild(card);

    }
}

//------------------------------------------------------
//FUNCTIONS that working with teacher information popup.
//------------------------------------------------------

//Function, that creates teacher card with providen teacher object
//teacher - teacher object with all data and properties
function getTeacherCard(teacher) {

    //Recieving card template and clonning it
    let teacherCardTemplate = document.querySelector("#template-teacher-card").content;
    let clonedCard = teacherCardTemplate.querySelector(".teacher-card").cloneNode(true);
    
    //Changing textContent of elements with teacher data.

    //Splitting full_name into name and surname, also using rest/spread operator becouse teacher can have multiple words
    let nameTags = clonedCard.querySelectorAll(".teacher-card-name");
    let teacherNames = teacher.full_name.split(" ");
    let [nameTeacher, ...surnameTeacher] = teacherNames;

    //Setting name and surname for a teacher card
    nameTags[0].textContent = nameTeacher;
    nameTags[1].textContent = String(surnameTeacher).replaceAll(",", " ");

    clonedCard.querySelector(".teacher-card-course").textContent = teacher.course;
    clonedCard.querySelector(".teacher-card-country").textContent = teacher.country;

    //Getting portrait element
    let portraitTag = clonedCard.querySelector(".portrait-container");

    //Attaching event listener to teacher portrait to open information popup
    portraitTag.addEventListener("click", () => {openTeacherCard(teacher)})

    //Setting portrait image src if teacher has it, otherwise we put initials
    if (teacher.picture_large) {

        portraitTag.querySelector("img").src = teacher.picture_large;

    } else {

        portraitTag.querySelector("img").remove();

        for (let i = 0; i < teacherNames.length; i++) {

            if (i < 2) portraitTag.textContent = portraitTag.textContent + (String(teacherNames[i][0])).toUpperCase() + ".";

        }

    }

    //If teacher isn't favorite, then removing a star
    if (!teacher.favorite) {

        clonedCard.querySelector(".favorite-teacher").remove();

    }

    return clonedCard;
}

//Function, that opens teacher information popup of provided teacher
//teacherObject - teacher object with all data and properties
function openTeacherCard(teacherObject) {
    
    //Recieving popup elements
    let teacherCardInfo = document.querySelector("#teacher-info");
    let favoriteButton = document.querySelector("#favorite-button");
    let closeButton = teacherCardInfo.querySelector(".close-popup");

    //Assigning new onclick events to buttons
    closeButton.onclick = function() {closePopup(teacherCardInfo); teacherMap.remove()};
    favoriteButton.onclick = function() {favoriteButtonListener(teacherObject)};

    //If teacher has image, then we set source link, otherwise we put paceholder
    if (teacherObject.picture_large) {
        teacherCardInfo.querySelector("#teacher-card-portrait").src = teacherObject.picture_large;
    } else {
        teacherCardInfo.querySelector("#teacher-card-portrait").src = "/images/ui-elements/no-image-icon.jpeg";
    }

    //Changing first letter of a gender string to uppercase
    let gender = teacherObject.gender;
    gender = gender.charAt(0).toUpperCase() + gender.slice(1);

    //Assigning teacher properties values to text content of according elements
    teacherCardInfo.querySelector("#teacher-card-name").textContent = teacherObject.full_name;
    teacherCardInfo.querySelector("#teacher-card-speciality").textContent = teacherObject.course;
    teacherCardInfo.querySelector("#teacher-card-location").textContent = teacherObject.city + ", " + teacherObject.country;
    teacherCardInfo.querySelector("#teacher-card-age").textContent = String(teacherObject.age) + ", " + gender;
    teacherCardInfo.querySelector("#teacher-card-tel").textContent = teacherObject.phone;

    //Calculating days till birthday
    
    let birthdateString = dayjs(teacherObject.b_date).format("DD-MM-YYYY");
    
    let today = dayjs();
    let nextBirthdate = dayjs(teacherObject.b_date).set('year', today.year());

    if (today.isAfter(nextBirthdate)) {
        nextBirthdate = nextBirthdate.add(1, 'year');
    }
    
    var daysLeft = nextBirthdate.diff(today, 'day');
    teacherCardInfo.querySelector("#teacher-card-birthday").textContent = `${birthdateString} (${daysLeft} days left)`;

    //Getting email element to change its multiple attributes
    let emailTag = teacherCardInfo.querySelector("#teacher-card-email");
    emailTag.href = "mailto:" + teacherObject.email;
    emailTag.textContent = teacherObject.email;

    //Getting teacher description element to change
    let teacherDescription = teacherCardInfo.querySelector("#teacher-card-description p");
    teacherDescription.textContent = teacherObject.note ? teacherObject.note : "No description provided.";

    //Changing favorite button state according to the value of property "favorite" of a teacher
    if (teacherObject.favorite) {

        teacherCardInfo.querySelector("#favorite-button img").src = "images/ui-elements/star-filled.svg";
        favoriteButton.dataset.state = "1";

    } else {

        teacherCardInfo.querySelector("#favorite-button img").src = "images/ui-elements/star-outline.svg";
        favoriteButton.dataset.state = "0";

    }

    //Setting correct map coordinates
    teacherMap = L.map('teacher-map').setView([teacherObject.coordinates.latitude, teacherObject.coordinates.longitude], 13);

    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(teacherMap);
    let marker = L.marker([teacherObject.coordinates.latitude, teacherObject.coordinates.longitude]).addTo(teacherMap)
        .bindPopup('I live here!');

    setTimeout(() => {
        teacherMap.invalidateSize();
        marker.openPopup();
    }, 0);

    //Displaying popup
    teacherCardInfo.style.display = "flex";
}

//Favorite button listener, that changes the states of a button
//teacherObject - teacher object with all data and properties
function favoriteButtonListener(teacherObject) {

    //Recieving button element
    const favoriteButton = document.querySelector("#favorite-button");

    //Depending on a current state of a button, we decide what to do
    if (favoriteButton.dataset.state == 0) {

        //We add a teacher to favorites only if it's not in the list already
        if (!containsTeacher(favoriteTeachers, teacherObject)) {

            favoriteTeachers.push(teacherObject);
            teacherObject.favorite = true;
            favoriteButton.querySelector("img").src = "images/ui-elements/star-filled.svg";
            favoriteButton.dataset.state = "1";

        } 

    } else {

        //We remove a teacher from favorites only if it's in the list
        if( containsTeacher(favoriteTeachers, teacherObject)) {

            favoriteTeachers = favoriteTeachers.filter((teacher) => teacher.full_name != teacherObject.full_name);
            teacherObject.favorite = false;
            favoriteButton.querySelector("img").src = "images/ui-elements/star-outline.svg";
            favoriteButton.dataset.state = "0"

        }
    }

    //Updating lists
    updateTeachersCarousel();
    updateTeachersList(topTeachers);
}

//----------------------------------------------
//FUNCTIONS that are working with add teacher form.
//----------------------------------------------

//Funtction, that is used to add teacher on form submit
function addTeacherOnSumbit() {

    //Recieving form element
    let form = document.querySelector("#add-teacher form");

    //Form fields
    let nameField = form.querySelector("#name-input");
    let courseField = form.querySelector("#speciality-input");
    let countryField = form.querySelector("#country-input");
    let cityField = form.querySelector("#city-input");
    let emailField = form.querySelector("#email-input");
    let phoneField = form.querySelector("#phone-input");
    let dateField = form.querySelector("#date-input");
    let sexField = form.querySelector("#sex-input");
    let colorField = form.querySelector("#color-input");
    let notesField = form.querySelector("#notes-input");

    //Removing classes from elements
    const textInputFields = [nameField, cityField, phoneField, dateField, notesField];
    
    for (let element of textInputFields) {
        clearClass(element, "invalid-data");
    }

    //Creating new teacher object and setting its properties
    let newTeacher = {
        "id": "",
        "favorite": false,
        "course": courseField.value,
        "bg_color": colorField.value,
        "note": notesField.value,
        "gender": sexField.querySelector('input[name="sex"]:checked').value,
        "title": "",
        "full_name": nameField.value.trim().replaceAll("/s+", " "),
        "city": cityField.value,
        "state": "State",
        "country": countryField.value,
        "postcode": 0,
        "coordinates": { "latitude": "0.0", "longitude": "0.0" },
        "timezone": { "offset": "0", "description": "UTC" },
        "email": emailField.value,
        "b_date": dateField.value,
        "age": countUserAge(new Date(dateField.value)),
        "phone": phoneField.value,
        "picture_large": "",
        "picture_thumbnail": "" 
    }

    //Checking if user is valid to add them to all teacher array
    if (isUserValid(newTeacher)) {

        if (!containsTeacher(allTeachers, newTeacher)) {
            
            allTeachers.push(newTeacher);
            //statisticsArray.push(newTeacher);
            topTeachers = applyAllFilters(allTeachers);

            loadStatsTable();
            
            updateTeachersList(topTeachers);

            try {
                sendFormData(form);
            } catch(error) {
                console.error("An error ocured while sending request to the server: ", error);
            }

            closePopup(document.querySelector("#add-teacher"));
            form.reset();

        } else {
            alert("User with this exact name and surname already exists.")
        }

    } else {

        highlightInvalidInputs(newTeacher);

        alert("Some of provided information is not valid.")

    }
}

function clearClass(element, className) {
    element.classList.remove(className);
}

function highlightInvalidInputs(teacherObject) {

    //Recieving form element
    let form = document.querySelector("#add-teacher form");

    //Form fields
    let nameField = form.querySelector("#name-input");
    let cityField = form.querySelector("#city-input");
    let phoneField = form.querySelector("#phone-input");
    let dateField = form.querySelector("#date-input");
    let notesField = form.querySelector("#notes-input");

    //If some inputs are invalid, then we apply class styling to this element
    if (!checkString(teacherObject.full_name)) nameField.classList.add("invalid-data");
    if (!checkString(teacherObject.city)) cityField.classList.add("invalid-data");
    if (!checkString(teacherObject.note)) notesField.classList.add("invalid-data");
    if (!isPhoneValid(teacherObject.phone)) phoneField.classList.add("invalid-data");
    if (!isDateValid(teacherObject.b_date)) dateField.classList.add("invalid-data");

}

//--------------
//MISC FUNCTIONS
//--------------

//Function, that shows teacher form popup
function showTeacherFormPopup() {

    let popup = document.querySelector("#add-teacher");
    popup.style.display = "flex";

}

//Function, that closes certain popup
//element - popup html element
function closePopup(element) {

    element.style.display = "none";

}

//Function, that checks of certain array has teacher in it
//array - array to look for teacherObject in
//teacherObject - teacher object to look for in array
function containsTeacher(array, teacherObject) {

    for (let teacher of array) {
        if (teacher.full_name == teacherObject.full_name) return true;
    }

    return false;

}

//Function, that counts age by given date
//birthdate - date of birth
function countUserAge(birthdate) {

    const today = new Date();
    const age = today.getFullYear() - birthdate.getFullYear() - 
                (today.getMonth() < birthdate.getMonth() || 
                (today.getMonth() === birthdate.getMonth() && today.getDate() < birthdate.getDate()));

    return Number(age);

}

//Function, that recieves filters values and returns filtered array
//array - array to filter
function getFilteredTeachers(array) {

    let filtered = [...array];

    let ageSelect = document.querySelector("#age");
    let regionSelect = document.querySelector("#region");
    let sexSelect = document.querySelector("#sex");
    let photoOnlyCheckbox = document.querySelector("#photo-only");
    let favoriteOnlyCheckbox = document.querySelector("#favorites-only");

    filtered = filterUsers(filtered, regionSelect.value, null, sexSelect.value, favoriteOnlyCheckbox.checked, photoOnlyCheckbox.checked);
    filtered = getExpressionArray(filtered, ageSelect.value);

    return filtered;

}

//Function, that applies not only list filters, but also a search filter.
//array - array to filter
function applyAllFilters(array) {
    let filtered = [...array];
    let searchField = document.querySelector("#search-field");

    if (isSearchApplied) {

        let regex = /\d+(?:[><]=?|-)\d+/;
        
        if (regex.test(searchField.value)) {
            filtered = getExpressionArray(filtered, searchField.value);
        } else {
            filtered = searchUsers(filtered, searchField.value);
        }
        
    }

    filtered = getFilteredTeachers(filtered);

    return filtered;
}